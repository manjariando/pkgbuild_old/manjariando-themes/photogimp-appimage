# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=photogimp-appimage
gimpver=2.10
_pkgver=1.0
pkgver=2020.${_pkgver}
pkgrel=6
arch=('x86_64')
license=('GPL-3.0')
url="https://github.com/Diolinux/PhotoGIMP"
pkgdesc="Photoshop theme for GIMP Appimage"
install=${pkgname}.install

__commit="edbde0689965f2d9a2c29b6df02ade4440d90e97"

source=("photogimp-${pkgver}.zip::https://github.com/Diolinux/PhotoGIMP/archive/${__commit}.zip"
        "https://metainfo.manjariando.com.br/photogimp/com.${pkgname}.metainfo.xml"
        'https://www.gnu.org/licenses/gpl-3.0.txt'
        "${pkgname}")
sha256sums=('541dc5baf60bcd2b3631f8c7a319e5c0c478d3090c3bdb2e6ce221cd88a19235'
            '873a20cf692e58e15297f0dd952fb123fe6c00599d9fb8e274900cb1168254af'
            '3972dc9744f6499f0f9b2dbf76696f2ae7ad8af9b23dde66d6af86c9dfb36986'
            '45f5a8005a42a3c723362c7961b18237c471e7d109a74617d8b3d2d2df2fedae')

_check_theme="PhotoGIMP
Version ${pkgver}"

_photogimp_desktop="[Desktop Entry]
Version=${pkgver}
Terminal=false
Type=Application
Name=PhotoGIMP Appimage
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_check_theme" | tee check.theme
    echo -e "$_photogimp_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    cd "${srcdir}/PhotoGIMP-${__commit}/.var/app/org.gimp.GIMP/config/GIMP/${gimpver}"

    install -Dm755 "${srcdir}/${pkgname}" "${pkgdir}/usr/bin/${pkgname}"
    mkdir -p "${pkgdir}/opt/${pkgname}/"{brushes,filters,internal-data,splashes,tool-options}
    cp -r {*rc,tags.xml,*-history} "${pkgdir}/opt/${pkgname}"
    cp -r {brushes,filters,fonts,internal-data,plug-ins,splashes,tool-options} "${pkgdir}/opt/${pkgname}"
    mv "${pkgdir}/opt/${pkgname}/brushes/Sem título.gbr" "${pkgdir}/opt/${pkgname}/brushes/Sem titulo.gbr"

    sed -i "s:GIMPVER=.*:GIMPVER=${gimpver}:" "${pkgdir}/usr/bin/${pkgname}" "${startdir}/${pkgname}.install"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/gpl-3.0.txt" "${pkgdir}/usr/share/licenses/${pkgname}/GPL3"
    install -Dm644 "${srcdir}/check.theme" "${pkgdir}/opt/${pkgname}"

    for i in 16 32 48 64 128 256 512; do
        install -Dm644 "${srcdir}/PhotoGIMP-${__commit}/.local/share/icons/hicolor/${i}x${i}/apps/photogimp.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
